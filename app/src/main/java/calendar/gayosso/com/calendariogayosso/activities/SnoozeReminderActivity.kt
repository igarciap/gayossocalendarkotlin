package calendar.gayosso.com.calendariogayosso.activities

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import calendar.gayosso.com.calendariogayosso.extensions.config
import calendar.gayosso.com.calendariogayosso.extensions.dbHelper
import calendar.gayosso.com.calendariogayosso.extensions.rescheduleReminder
import calendar.gayosso.com.calendariogayosso.extensions.showPickIntervalDialog
import calendar.gayosso.com.calendariogayosso.helpers.EVENT_ID

class SnoozeReminderActivity : AppCompatActivity() {
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showPickIntervalDialog(config.snoozeTime, true, cancelCallback = { dialogCancelled() }) {
            val eventId = intent.getIntExtra(EVENT_ID, 0)
            val event = dbHelper.getEventWithId(eventId)
            config.snoozeTime = it
            rescheduleReminder(event, it)
            finishActivity()
        }
    }

    private fun dialogCancelled() {
        finishActivity()
    }

    private fun finishActivity() {
        finish()
        overridePendingTransition(0, 0)
    }
}
