package calendar.gayosso.com.calendariogayosso.interfaces

import android.util.SparseArray
import calendar.gayosso.com.calendariogayosso.models.DayYearly
import java.util.*

interface YearlyCalendar {
    fun updateYearlyCalendar(events: SparseArray<ArrayList<DayYearly>>, hashCode: Int)
}
