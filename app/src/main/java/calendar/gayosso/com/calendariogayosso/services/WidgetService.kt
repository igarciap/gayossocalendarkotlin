package calendar.gayosso.com.calendariogayosso.services

import android.content.Intent
import android.widget.RemoteViewsService
import calendar.gayosso.com.calendariogayosso.adapters.EventListWidgetAdapter

class WidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent) = EventListWidgetAdapter(applicationContext)
}
