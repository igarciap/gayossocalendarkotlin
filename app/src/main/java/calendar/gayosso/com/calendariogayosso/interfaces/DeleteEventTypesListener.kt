package calendar.gayosso.com.calendariogayosso.interfaces

import calendar.gayosso.com.calendariogayosso.models.EventType
import java.util.*

interface DeleteEventTypesListener {
    fun deleteEventTypes(eventTypes: ArrayList<EventType>, deleteEvents: Boolean): Boolean
}
