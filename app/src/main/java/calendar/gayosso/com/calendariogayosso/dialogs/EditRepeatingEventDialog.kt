package calendar.gayosso.com.calendariogayosso.dialogs

import android.support.v7.app.AlertDialog
import android.view.ViewGroup
import calendar.gayosso.com.calendariogayosso.R
import calendar.gayosso.com.calendariogayosso.activities.SimpleActivity
import calendar.gayosso.com.calendariogayosso.extensions.setupDialogStuff
import com.simplemobiletools.commons.extensions.hideKeyboard
import kotlinx.android.synthetic.main.dialog_edit_repeating_event.view.*

class EditRepeatingEventDialog(val activity: SimpleActivity, val callback: (allOccurrences: Boolean) -> Unit) {
    var dialog: AlertDialog

    init {
        val view = (activity.layoutInflater.inflate(R.layout.dialog_edit_repeating_event, null) as ViewGroup).apply {
            edit_repeating_event_one_only.setOnClickListener { sendResult(false) }
            edit_repeating_event_all_occurrences.setOnClickListener { sendResult(true) }
        }

        dialog = AlertDialog.Builder(activity)
                .create().apply {
                    activity.setupDialogStuff(view, this) {
                        hideKeyboard()
                    }
                }
    }

    private fun sendResult(allOccurrences: Boolean) {
        callback(allOccurrences)
        dialog.dismiss()
    }
}
