package calendar.gayosso.com.calendariogayosso.interfaces

import calendar.gayosso.com.calendariogayosso.models.Event

interface WeeklyCalendar {
    fun updateWeeklyCalendar(events: ArrayList<Event>)
}
