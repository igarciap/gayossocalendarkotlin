package calendar.gayosso.com.calendariogayosso.receivers

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import calendar.gayosso.com.calendariogayosso.extensions.notifyRunningEvents
import calendar.gayosso.com.calendariogayosso.extensions.recheckCalDAVCalendars
import calendar.gayosso.com.calendariogayosso.extensions.scheduleAllEvents

class BootCompletedReceiver : BroadcastReceiver() {

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceive(context: Context, arg1: Intent) {
        context.apply {
            scheduleAllEvents()
            notifyRunningEvents()
            recheckCalDAVCalendars {}
        }
    }
}
