package calendar.gayosso.com.calendariogayosso.interfaces

import android.content.Context
import calendar.gayosso.com.calendariogayosso.models.DayMonthly

interface MonthlyCalendar {
    fun updateMonthlyCalendar(context: Context, month: String, days: List<DayMonthly>, checkedEvents: Boolean)
}
