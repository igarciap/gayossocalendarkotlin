package calendar.gayosso.com.calendariogayosso.receivers

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import android.support.annotation.RequiresApi
import calendar.gayosso.com.calendariogayosso.extensions.dbHelper
import calendar.gayosso.com.calendariogayosso.extensions.notifyEvent
import calendar.gayosso.com.calendariogayosso.extensions.scheduleAllEvents
import calendar.gayosso.com.calendariogayosso.extensions.updateListWidget
import calendar.gayosso.com.calendariogayosso.helpers.EVENT_ID
import calendar.gayosso.com.calendariogayosso.helpers.Formatter

class NotificationReceiver : BroadcastReceiver() {
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceive(context: Context, intent: Intent) {
        val powerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Simple Calendar")
        wakelock.acquire(5000)

        context.updateListWidget()
        val id = intent.getIntExtra(EVENT_ID, -1)
        if (id == -1) {
            return
        }

        val event = context.dbHelper.getEventWithId(id)
        if (event == null || event.getReminders().isEmpty()) {
            return
        }

        if (!event.ignoreEventOccurrences.contains(Formatter.getDayCodeFromTS(event.startTS).toInt())) {
            context.notifyEvent(event)
        }
        context.scheduleAllEvents()
    }
}
