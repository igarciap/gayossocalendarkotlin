package calendar.gayosso.com.calendariogayosso.services

import android.annotation.TargetApi
import android.app.IntentService
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import calendar.gayosso.com.calendariogayosso.extensions.config
import calendar.gayosso.com.calendariogayosso.extensions.dbHelper
import calendar.gayosso.com.calendariogayosso.extensions.rescheduleReminder
import calendar.gayosso.com.calendariogayosso.helpers.EVENT_ID

class SnoozeService : IntentService("Snooze") {
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onHandleIntent(intent: Intent) {
        val eventId = intent.getIntExtra(EVENT_ID, 0)
        val event = dbHelper.getEventWithId(eventId)
        rescheduleReminder(event, config.snoozeTime)
    }
}
