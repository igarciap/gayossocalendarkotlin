package calendar.gayosso.com.calendariogayosso.helpers

import android.content.Context
import calendar.gayosso.com.calendariogayosso.extensions.dbHelper
import calendar.gayosso.com.calendariogayosso.interfaces.WeeklyCalendar
import calendar.gayosso.com.calendariogayosso.models.Event
import java.util.*

class WeeklyCalendarImpl(val mCallback: WeeklyCalendar, val mContext: Context) {
    var mEvents = ArrayList<Event>()

    fun updateWeeklyCalendar(weekStartTS: Int) {
        val startTS = weekStartTS
        val endTS = startTS + WEEK_SECONDS
        mContext.dbHelper.getEvents(startTS, endTS) {
            mEvents = it as ArrayList<Event>
            mCallback.updateWeeklyCalendar(it)
        }
    }
}
