package calendar.gayosso.com.calendariogayosso.models

data class RepeatRule(val repeatInterval: Int, val repeatRule: Int, val repeatLimit: Int)
